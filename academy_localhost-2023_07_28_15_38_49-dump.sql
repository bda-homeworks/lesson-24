-- MySQL dump 10.13  Distrib 8.0.34, for Win64 (x86_64)
--
-- Host: 127.0.0.1    Database: academy
-- ------------------------------------------------------
-- Server version	8.1.0

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!50503 SET NAMES utf8mb4 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `staff`
--

DROP TABLE IF EXISTS `staff`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `staff` (
  `Id` int DEFAULT NULL,
  `Name` varchar(50) DEFAULT NULL,
  `Surname` varchar(50) DEFAULT NULL,
  `Position` varchar(50) DEFAULT NULL,
  `Salary` int DEFAULT NULL,
  `Start_date` varchar(50) DEFAULT NULL,
  `Leave_date` varchar(50) DEFAULT NULL,
  `Create_date` varchar(50) DEFAULT NULL,
  `Update_date` varchar(50) DEFAULT NULL,
  `Status` varchar(50) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `staff`
--

LOCK TABLES `staff` WRITE;
/*!40000 ALTER TABLE `staff` DISABLE KEYS */;
INSERT INTO `staff` VALUES (1,'John','Doe','Manager',60000,'2020-01-15','2023-07-15','2021-08-10','2023-07-27','Active'),(2,'Jane','Smith','Assistant',45000,'2019-07-02','2023-06-30','2020-03-20','2023-07-27','Active'),(3,'Michael','Johnson','Developer',70000,'2022-03-18','2023-07-27','2022-02-05','2023-07-27','Active'),(4,'Emily','Brown','Designer',55000,'2021-11-22','2023-07-27','2022-01-30','2023-07-27','Active'),(5,'Alice','Wilson','Manager',58000,'2018-09-05','2023-07-27','2019-01-12','2023-07-27','Active'),(6,'David','Lee','Developer',72000,'2023-01-01','2023-07-27','2022-11-10','2023-07-27','Active'),(7,'Sophia','Clark','Assistant',48000,'2022-08-11','2023-07-27','2022-07-01','2023-07-27','Active'),(8,'James','Anderson','Manager',62000,'2020-06-25','2023-07-27','2020-04-20','2023-07-27','Active'),(9,'Olivia','White','Developer',69000,'2023-04-12','2023-07-27','2023-03-05','2023-07-27','Active'),(10,'William','Martin','Designer',52000,'2019-12-07','2023-07-27','2020-02-15','2023-07-27','Active'),(11,'Ava','Harris','Manager',61000,'2018-05-30','2023-07-27','2019-01-18','2023-07-27','Active'),(12,'Joseph','Miller','Developer',69000,'2022-11-15','2023-07-27','2023-01-10','2023-07-27','Active'),(13,'Sophia','Thomas','Assistant',47000,'2021-09-20','2023-07-27','2021-03-08','2023-07-27','Active'),(14,'Michael','Moore','Designer',54000,'2023-02-05','2023-07-27','2023-01-20','2023-07-27','Active'),(15,'Emma','Jackson','Manager',59000,'2020-08-14','2023-07-27','2021-02-22','2023-07-27','Active'),(16,'Daniel','Taylor','Developer',71000,'2022-06-10','2023-07-27','2022-02-28','2023-07-27','Active'),(17,'Olivia','Anderson','Designer',53000,'2019-02-23','2023-07-27','2019-01-20','2023-07-27','Active'),(18,'Liam','Johnson','Manager',60000,'2021-01-05','2023-07-27','2021-08-10','2023-07-27','Active'),(19,'Mia','Wilson','Developer',67000,'2018-04-22','2023-07-27','2019-03-20','2023-07-27','Active'),(20,'Ethan','Brown','Designer',54000,'2020-03-18','2023-07-27','2022-02-05','2023-07-27','Active');
/*!40000 ALTER TABLE `staff` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `student`
--

DROP TABLE IF EXISTS `student`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `student` (
  `Id` int DEFAULT NULL,
  `Name` varchar(50) DEFAULT NULL,
  `Surname` varchar(50) DEFAULT NULL,
  `Subject` varchar(50) DEFAULT NULL,
  `Monthly_payment` int DEFAULT NULL,
  `Start_date` varchar(50) DEFAULT NULL,
  `Leave_date` varchar(50) DEFAULT NULL,
  `Create_date` varchar(50) DEFAULT NULL,
  `Update_date` varchar(50) DEFAULT NULL,
  `Status` varchar(50) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `student`
--

LOCK TABLES `student` WRITE;
/*!40000 ALTER TABLE `student` DISABLE KEYS */;
INSERT INTO `student` VALUES (1,'Emma','Johnson','Mathematics',200,'2020-09-01','2023-07-28','2023-07-28','2023-07-28','Active'),(2,'Noah','Williams','English',180,'2021-02-15','2023-07-28','2023-07-28','2023-07-28','Active'),(3,'Olivia','Smith','Science',210,'2019-11-10','2023-07-28','2023-07-28','2023-07-28','Active'),(4,'Liam','Brown','Computer Science',190,'2022-06-05','2023-07-28','2023-07-28','2023-07-28','Active'),(5,'Ava','Jones','Music',175,'2023-01-15','2023-07-28','2023-07-28','2023-07-28','Active'),(6,'Isabella','Miller','Art',160,'2020-07-02','2023-07-28','2023-07-28','2023-07-28','Active'),(7,'Sophia','Wilson','Physical Education',190,'2022-04-22','2023-07-28','2023-07-28','2023-07-28','Active'),(8,'Mia','Taylor','Geography',180,'2021-09-18','2023-07-28','2023-07-28','2023-07-28','Active'),(9,'Amelia','Anderson','Chemistry',220,'2018-08-09','2023-07-28','2023-07-28','2023-07-28','Active'),(10,'Harper','Thomas','Foreign Language',195,'2022-03-14','2023-07-28','2023-07-28','2023-07-28','Active'),(11,'Evelyn','Jackson','Biology',210,'2023-01-28','2023-07-28','2023-07-28','2023-07-28','Active'),(12,'Abigail','White','Economics',200,'2020-11-11','2023-07-28','2023-07-28','2023-07-28','Active'),(13,'Emily','Martin','Physics',225,'2019-06-30','2023-07-28','2023-07-28','2023-07-28','Active'),(14,'Elizabeth','Lee','Social Studies',185,'2022-03-17','2023-07-28','2023-07-28','2023-07-28','Active'),(15,'Sofia','Scott','Political Science',200,'2021-01-03','2023-07-28','2023-07-28','2023-07-28','Active'),(16,'Avery','Young','Literature',190,'2018-09-28','2023-07-28','2023-07-28','2023-07-28','Active'),(17,'Ella','Hall','Drama',175,'2023-06-20','2023-07-28','2023-07-28','2023-07-28','Active'),(18,'Scarlett','King','Physical Science',200,'2020-11-25','2023-07-28','2023-07-28','2023-07-28','Active'),(19,'Grace','Adams','Computer Programming',220,'2019-07-08','2023-07-28','2023-07-28','2023-07-28','Active'),(20,'Chloe','Harris','History',190,'2021-05-12','2023-07-28','2023-07-28','2023-07-28','Active');
/*!40000 ALTER TABLE `student` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `teacher`
--

DROP TABLE IF EXISTS `teacher`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `teacher` (
  `Id` int DEFAULT NULL,
  `Name` varchar(50) DEFAULT NULL,
  `Surname` varchar(50) DEFAULT NULL,
  `Subject` varchar(50) DEFAULT NULL,
  `Salary` int DEFAULT NULL,
  `Start_date` varchar(50) DEFAULT NULL,
  `Leave_date` varchar(50) DEFAULT NULL,
  `Create_date` varchar(50) DEFAULT NULL,
  `Update_date` varchar(50) DEFAULT NULL,
  `Status` varchar(50) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `teacher`
--

LOCK TABLES `teacher` WRITE;
/*!40000 ALTER TABLE `teacher` DISABLE KEYS */;
INSERT INTO `teacher` VALUES (2,'Jane','Doe','English',48000,'2019-09-15','2023-07-28','2023-07-28','2023-07-28','Active'),(3,'Michael','Johnson','History',55000,'2021-03-10','2023-07-28','2023-07-28','2023-07-28','Active'),(4,'Emily','Williams','Science',52000,'2022-06-05','2023-07-28','2023-07-28','2023-07-28','Active'),(5,'Christopher','Brown','Computer Science',58000,'2018-11-20','2023-07-28','2023-07-28','2023-07-28','Active'),(6,'Jennifer','Jones','Music',45000,'2023-01-15','2023-07-28','2023-07-28','2023-07-28','Active'),(7,'Daniel','Miller','Art',47000,'2020-07-02','2023-07-28','2023-07-28','2023-07-28','Active'),(8,'Lisa','Wilson','Physical Education',51000,'2019-04-22','2023-07-28','2023-07-28','2023-07-28','Active'),(9,'David','Taylor','Geography',49000,'2022-02-18','2023-07-28','2023-07-28','2023-07-28','Active'),(10,'Mary','Anderson','Chemistry',60000,'2018-08-09','2023-07-28','2023-07-28','2023-07-28','Active'),(11,'Richard','Thomas','Foreign Language',53000,'2021-12-14','2023-07-28','2023-07-28','2023-07-28','Active'),(12,'Karen','Jackson','Biology',57000,'2023-03-28','2023-07-28','2023-07-28','2023-07-28','Active'),(13,'Matthew','White','Economics',54000,'2020-10-11','2023-07-28','2023-07-28','2023-07-28','Active'),(14,'Amanda','Martin','Physics',59000,'2019-06-30','2023-07-28','2023-07-28','2023-07-28','Active'),(15,'Kevin','Lee','Social Studies',46000,'2022-04-17','2023-07-28','2023-07-28','2023-07-28','Active'),(16,'Laura','Scott','Political Science',51000,'2021-02-03','2023-07-28','2023-07-28','2023-07-28','Active'),(17,'Jason','Young','Literature',50000,'2018-09-28','2023-07-28','2023-07-28','2023-07-28','Active'),(18,'Sarah','Hall','Drama',47000,'2023-06-20','2023-07-28','2023-07-28','2023-07-28','Active'),(19,'Eric','King','Physical Science',54000,'2020-11-25','2023-07-28','2023-07-28','2023-07-28','Active'),(20,'Michelle','Adams','Computer Programming',58000,'2019-07-08','2023-07-28','2023-07-28','2023-07-28','Active');
/*!40000 ALTER TABLE `teacher` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2023-07-28 15:38:49
